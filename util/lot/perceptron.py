import sys 
from keras.models import load_model
from tqdm.auto import tqdm
from .multi_metric import *




pbar = None
def train_perceptron(module, activaction, properties, Y, metric_mod, epoch_measure):
    if(metric_mod is None):
        raise Exception("Error, no se definio la el modulo para la metrica.")
    func = str(metric_mod)
    return eval(('module.%s(activaction, properties, Y, epoch_measure)' % (func)))

def import_from(s):
    module = __import__(s)
    return module

def perceptron_executor(metric, activaction, properties, Y):
    if(metric["lot"] is None):
        raise Exception("Error, no posee cantidad de lote.")
    
    # Arreglo con los intervalos de epochs utilizados en la ejecución
    inter = range(metric["quantity"])

    if(properties.method_metric() is None):
        raise Exception("Error, no posee el metodo de ejecucion.")
    
    if properties.method_metric() == 'parallel':
        results = multi_predict(len(inter), activaction, metric["parameters"], Y, metric["module"], metric["epoch_measure"])

    else: 
        if properties.method_metric() == 'sequential':
            func = str(metric["module"])
            module = import_from(func)

            results = [{} for x in inter]
            for i in tqdm(range(len(inter))):
                results[i] = train_perceptron(module, activaction, metric["parameters"], Y, metric["module"], metric["epoch_measure"])
    return results

    