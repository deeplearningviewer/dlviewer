from keras.models import load_model
from util.lot.perceptron import perceptron_executor

def metricCalculation(perceptron, input_total, measure):
    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")
    
    n = len(perceptron)
    if measure == "last":
        me = perceptron[-1]
    else:
        if measure == "prom":
            minimo = (sum(perceptron) / n)
            me = 100 - (minimo*100/input_total)
        else:
            if measure == "max":
                perceptron.sort()
                cant = perceptron[-1]
                me = 100 - (cant*100/input_total)
            else:
                if measure == "less":
                    perceptron.sort()
                    cant = perceptron[0]
                    me = 100 - (cant*100/input_total)
                else:
                    minimo = (sum(perceptron) / n)
                    me = 100 - (minimo*100/input_total)
    return me

def lot_executor(activaction ,properties, Y):
    metrics = properties.metrics()
    metric_results = []
    metric_name = []
    for metric in metrics:
        if(metric["lot"] is None): 
            raise Exception("Error, no posee cantidad de lote.")
    
        # Arreglo con los intervalos de epochs utilizados en la ejecución
        inter = range(metric["lot"])
        results = [{} for x in inter]
        c = len(inter)

        print('lot %s ---- quantity: %s' % (c, metric["quantity"]))
        for i in range(c):
            l = perceptron_executor(metric, activaction, properties, Y)
            n = metricCalculation(l, len(Y), metric["lot_measure"])
            results[i] = n

        metric_name.append(metric["module"])
        metric_results.append(results)

    return metric_name, metric_results