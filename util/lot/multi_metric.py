from tqdm import tqdm


def import_from(s):
    module = __import__(s)
    return module

def train_multi_perceptron(activaction, properties, Y, metric_mod, epoch_measure):
    if(metric_mod is None):
        raise Exception("Error, no se definio la el modulo para la metrica.")

    metric = import_from(metric_mod)
    func = str(metric_mod)
    return eval(('metric.%s(activaction, properties, Y, epoch_measure)' % (func)))


from .metric_pool import *
def multi_predict(n, activaction, properties, Y, metric_mod, epoch_measure):
    p = get_Pool()
    pbar = tqdm(total=n)
    res = [p.apply_async(train_multi_perceptron, args=(
        activaction, properties, Y, metric_mod, epoch_measure), callback=lambda _: pbar.update(1)) for i in range(n)]

    p.close()
    p.join()
    pbar.close()
    results = [p.get() for p in res]

    return results