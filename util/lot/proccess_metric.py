import multiprocessing as mp
from tqdm import tqdm


def import_from(s):
    module = __import__(s)
    return module

def train_proccess_perceptron(pos, output, lock, pbar, activaction, properties, Y):
    metric = import_from(properties.module())
    if(properties.module() is None):
        raise Exception("Error, no se definio la el modulo para la metrica.")
    func = str(properties.module())
    output.put((pos, eval(('metric.%s(activaction, properties, Y)' % (func)))))
    lock.acquire()
    pbar.update(1)
    lock.release()
    #return eval(('metric.%s(activaction, properties, Y)' % (func)))

def proccess_predict(n, activaction, properties, Y):
    # Define an output queue
    output = mp.Queue()
    pbar = tqdm(total = n)
    lock = mp.Lock()
    # Setup a list of processes that we want to run
    processes = [mp.Process(target=train_proccess_perceptron, 
        args=(x, output, lock, pbar, activaction, properties, Y)) for x in range(n)]

    # Run processes
    for p in processes:
        p.start()

    # Exit the completed processes
    for p in processes:
        p.join()

    pbar.close()
    # Get process results from the output queue
    results = [output.get() for p in processes]

    results.sort()
    res = [r[1] for r in results]

    return res