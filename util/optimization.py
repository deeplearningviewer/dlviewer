from keras.models import load_model
import tensorflow.python.keras.backend as K
from keras.models import Model
from keras.models import Sequential
from util.activaction.activaction import get_activaction_Model
from util.construct_dnn import *
from keras.layers import Dense
from keras import regularizers
import tensorflow as tf

def get_accurancy (exe_amount, modelName, properties, X, Y):
    models = load_model('data/check/'+modelName+'_'+exe_amount+'.h5')
    models.load_weights('data/check/weights_'+exe_amount+'.hdf5')
    scores = dnn_test(models, properties, X, Y)
    acc_temp = scores[1]*100
    return acc_temp, models

def get_layer(model):
    cont = 0
    return_layer = None
    for layer in model.layers:
        if cont == 0:
            return_layer = layer
            break
    return return_layer

def get_last_layer(model):
    cont = 0
    return_layer = None
    for layer in model.layers:
        if cont == 1:
            return_layer = layer
            break
        cont = cont +1
    return return_layer


def optimization(exe_amount, mode, properties, Y_train, Y_test, X_train, X_test, originalModel):
    if(mode == 'train-test'):
        X = X_test
        Y = Y_test
    else:
        if(mode == 'train-train'):
            X = X_train
            Y = Y_train
    accurancy, model = get_accurancy (exe_amount,'my_model', properties, X, Y)
    cont = 0
    last = len(model.layers) -1
    activaction_train = None
    activaction_test = None
    optimization_model = Sequential()
    for layer in model.layers:
        if cont < last:
            print("cont: "+ str(cont))
            if cont == 0:
                layer_model = Sequential()
                if properties.optimization_reset_weight_bias() == True:
                    layer.kernel.assign(layer.kernel_initializer(tf.shape(layer.kernel)))
                    layer.bias.assign(layer.bias_initializer(tf.shape(layer.bias)))
                layer_model.add(layer)
                layer_model.add(Dense(1, activation='sigmoid'))
                layer_model.compile(loss=properties.loss(), optimizer=properties.optimizer(), metrics=['accuracy'])
                layer_model.fit(X_train, Y_train, epochs=properties.epochs()*2, batch_size=properties.batch_size(), verbose=2)
                print(layer_model.summary())
                scores = dnn_test(layer_model, properties, X, Y)
                acc_temp = scores[1]*100
                print("Layer :"+ str(cont+1)+ " - accurancy: "+ str(acc_temp))
                activaction_train = get_activaction_Model(layer_model, cont, X_train)
                activaction_test = get_activaction_Model(layer_model, cont, X)
                optimization_model.add(get_layer(layer_model))
            if cont > 0:
                layer_model = Sequential()
                if properties.optimization_reset_weight_bias() == True:
                    layer.kernel.assign(layer.kernel_initializer(tf.shape(layer.kernel)))
                    layer.bias.assign(layer.bias_initializer(tf.shape(layer.bias)))
                layer_model.add(layer)
                layer_model.add(Dense(1, activation='sigmoid'))
                layer_model.compile(loss=properties.loss(), optimizer=properties.optimizer(), metrics=['accuracy'])
                layer_model.fit(activaction_train, Y_train, epochs=properties.epochs()*2, batch_size=properties.batch_size(), verbose=2)
                print(layer_model.summary())
                scores = dnn_test(layer_model, properties, activaction_test, Y)
                acc_temp = scores[1]*100
                print("Layer :"+ str(cont+1)+ " - accurancy: "+ str(acc_temp))
                activaction_train = get_activaction_Model(layer_model, cont, activaction_train)
                activaction_test = get_activaction_Model(layer_model, cont, activaction_test)
                optimization_model.add(get_layer(layer_model))
                if cont == (last -1):
                    optimization_model.add(get_last_layer(layer_model))
            cont = cont + 1
    optimization_model.compile(loss=properties.loss(), optimizer=properties.optimizer(), metrics=['accuracy'])
    optimization_model.build()
    print(optimization_model.summary())
    scores = dnn_test(optimization_model, properties, X, Y)
    acc_temp = scores[1]*100
    print("Accurancy original :"+ str(accurancy))
    print("Accurancy Optimizado :"+ str(acc_temp))
    model.save('data/check/optimization_model_'+exe_amount+'.h5')