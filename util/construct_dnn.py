import sys
from keras.models import Sequential
from keras.callbacks import Callback
from keras.callbacks import ModelCheckpoint

def construct_dnn(exe_amount, properties=None, wsave=None, shape=None, X_train=None, Y_train=None):
    layers = properties.dnn()
    architecture = properties.model_architecture()

    if(layers is None):
        raise Exception("Error, el modelo de la aplicacion no se encuentra cargado.")

    if(architecture is None):
        raise Exception("Error, la architecture de la aplicacion no se encuentra cargado.")
    
    model = Sequential()
    cont = 0
    sys.path.append('./util/layers')
    sys.path.append('./util/metric')
    for layer in layers:
        try:
            #exec('from util.layers.%s import *' % (architecture[cont])) 
            #eval('%s(layer)' % (architecture[cont]))
            func = str(architecture[cont])
            module = import_from(func)
            if(func == "conv" or func == "fc"):
                if(cont != 0):
                    shape = None
                model.add(eval('module.%s(layer, shape)' % (func)))
            else:
                model.add(eval('module.%s(layer)' % (func)))
            cont = cont + 1
        except:
            raise Exception("Error, no se puedo generar dinamicamente el modelo.")
    if properties.weights() == "same":
        if not (wsave):
            print("Se guarda los pesos de la primera ejecucion.")
            wsave=model.get_weights()
        else:
            print("Se carga los pesos para la ejecución.")
            model.set_weights(wsave)
    
    # Se imprime la estructura la arquitectura
    print(model.summary())
    # Se agrega el fiscalizador de intervalo
    if(properties.metric_enable() == True):
        weight_save_callback = ModelCheckpoint('data/check/weights_'+exe_amount+'.{epoch:02d}.hdf5', monitor='val_loss', verbose=0, save_best_only=False, mode='auto', period=properties.interval())
    if(properties.layering_optimization() == True):
        weight_save_callback = ModelCheckpoint('data/check/weights_'+exe_amount+'.hdf5', monitor='val_loss', verbose=0, save_best_only=False, mode='auto')
    # Se compila el modelo
    model.compile(loss=properties.loss(), optimizer=properties.optimizer(), metrics=['accuracy'])
    # Se entrena el modelo
    model.fit(X_train, Y_train, epochs=properties.epochs(), batch_size=properties.batch_size(), verbose=2,callbacks=[weight_save_callback])
    
    model.save('data/check/my_model_'+exe_amount+'.h5')

    return model, wsave

def import_from(s):
    module = __import__(s)
    return module

def dnn_test(model, properties, X, Y):
    if(model is None):
        raise Exception("Error, no se encuentra cargado el modelo.")

    if(X is None):
        raise Exception("Error, no se encuentra cargado el dataset X.")

    if(Y is None):
        raise Exception("Error, no se encuentra cargado el dataset Y.")
    
    # Se compila el modelo
    #model.compile(loss=properties.loss(), optimizer=properties.optimizer(), metrics=['accuracy'])
    # evaluate the model
    scores = model.evaluate(X, Y)
    return scores