from time import time
import math
import csv
import os
import matplotlib.pyplot as plt

def resultUniqui(i, interval, acc, layers, metric_name, r, elapsed_time):
    if os.path.isfile('data/result/net_result_'+str(i)+'.csv'):
        os.remove('data/result/net_result_'+str(i)+'.csv')

    final_time = "%0.10f" % elapsed_time
    time_head = ['Elapsed time: ', str(str(final_time)+' seconds')]

    with open('data/result/net_result_'+str(i)+'.csv', 'a') as f:
        writer = csv.writer(f)
        
        head = ['DNN','Accuracy']
        writer.writerow(head)
        
        #=== epoch - accuracy ===
        cont_epoch = 0
        for a in acc:
            accf = [str('epoch '+ str(interval[cont_epoch])), str(a)]
            writer.writerow(accf)
            cont_epoch += 1
        #=== epoch - accuracy ===

        writer.writerow([])
        #=== Metrics Measurements ===
        writer.writerow(['Metrics Measurements'])
        cont_metric = 0
        for m_name in metric_name[0][0]:
            writer.writerow(['Metric: ',str(m_name)])
            cont_layer = 0
            for lay in layers[0]:
                writer.writerow([str(lay+ ' Hidden layer ')])
                cont_interval = 0

                contador = 0
                acumulador = 0
                mes =[]
                for inter in interval:
                    metric_value = r[cont_interval][cont_layer][cont_metric]
                    
                    m = []
                    m.append(str('epoch '+ str(inter)))
                    #=== lot - value ===
                    x = len(metric_value)
                    if x > 1:
                        me = 0
                        for lot in metric_value:
                            m.append(str(lot))
                            me = abs(me - lot)
                        m.append(str(me))
                        
                        acumulador += me
                        mes.append(me)
                        contador += 1

                    #=== lot - value ===

                    writer.writerow(m)
                    cont_interval += 1

                promedio = acumulador / contador
                nn = 0
                for mmm in mes:
                    nn += pow((mmm-promedio),2)
                varianza = nn / contador
                desviacion_estandar = math.sqrt(varianza) 

                writer.writerow(['Average: ', str(promedio)])
                writer.writerow(['Standard Deviation: ', str(desviacion_estandar)])
                writer.writerow([])

                cont_layer += 1
            cont_metric += 1
        #=== Metrics Measurements ===

        writer.writerow([])
        writer.writerow(time_head)

def resultMeasuresFiles(i, interval, acc, layers, metric_name, r, elapsed_time):
    if os.path.isfile('data/result/net_result_'+str(i)+'.csv'):
        os.remove('data/result/net_result_'+str(i)+'.csv')

    final_time = "%0.10f" % elapsed_time
    time_head = ['Elapsed time: ', str(str(final_time)+' seconds')]

    with open('data/result/net_result_'+str(i)+'.csv', 'a') as f:
        writer = csv.writer(f)
        
        head = ['DNN','Accuracy']
        writer.writerow(head)
        
        #=== epoch - accuracy ===
        cont_epoch = 0
        for a in acc:
            accf = [str('epoch '+ str(interval[cont_epoch])), str(a)]
            writer.writerow(accf)
            cont_epoch += 1
        #=== epoch - accuracy ===

        writer.writerow([])
        #=== Metrics Measurements ===
        writer.writerow(['Metrics Measurements'])
        cont_metric = 0
        for m_name in metric_name[0][0]:
            writer.writerow(['Metric: ',str(m_name)])
            cont_layer = 0
            for lay in layers[0]:
                writer.writerow([str(lay+ ' Hidden layer ')])
                cont_interval = 0

                mes =[]
                for inter in interval:
                    metric_value = r[cont_interval][cont_layer][cont_metric]

                    m = []
                    m.append(str('epoch '+ str(inter)))
                    #=== lot - value ===
                    x = len(metric_value)
                    if x > 0:
                        me = 0
                        for lot in metric_value:
                            m.append(str(lot))
                    #=== lot - value ===
                    writer.writerow(m)
                    cont_interval += 1

                writer.writerow([])

                cont_layer += 1
            cont_metric += 1
        #=== Metrics Measurements ===

        writer.writerow([])
        writer.writerow(time_head)  

