from keras.models import load_model
from util.activaction.activaction import get_activaction_Model
from util.lot.lot import lot_executor
from util.construct_dnn import *
import os
import csv
import pandas


def get_model (modelName, epoch):
    models = load_model('data/check/'+modelName+'.h5')
    models.load_weights('data/check/weights.'+(str(epoch).zfill(2))+'.hdf5')
    return models

def get_accurancy (exe_amount, modelName, epoch, properties, X, Y):
    models = load_model('data/check/'+modelName+'_'+exe_amount+'.h5')
    models.load_weights('data/check/weights_'+exe_amount+'.'+(str(epoch).zfill(2))+'.hdf5')
    scores = dnn_test(models, properties, X, Y)
    acc_temp = scores[1]*100
    return acc_temp, models

def import_from(s):
    module = __import__(s)
    return module

def convertY20_1(Y):
    n = []
    for i in Y:
        if i == 0:
            n.append(-1)
        else:
            n.append(1)
    return n

def metric(epoch, properties, X, Y, model):
    #model = get_model ('my_model', epoch)
    l = len(model.layers)
    layer_name = []
    metric_activaction_name = []
    layer_activaction = []
    cont = 0
    last = len(model.layers) -1
    ban = True

    Y = convertY20_1(Y)
    for layer in model.layers:
        if cont < last:
            layer_type = layer.__class__.__name__
            if properties.metric_type() == "layerings":
                if layer_type in properties.metric_type_array():
                    ban = True
                else:
                    ban = False
            else:
                ban = True

            if ban == True:
                activaction = get_activaction_Model(model, cont, X)
                #print('epoch= %s ---- activaction = %s ---- shape = %s' % (epoch, cont, activaction.shape))
                if os.path.isfile('data/activation/hidden_'+str(cont+1)+'-'+str(epoch)+'_activations.csv'):
                    os.remove('data/activation/hidden_'+str(cont+1)+'-'+str(epoch)+'_activations.csv')
                #input('conti 1')
                df = pandas.DataFrame(activaction)
                dfy = pandas.DataFrame(Y)
                df_join = df.join(dfy, lsuffix='0_exec', rsuffix='1_exec')
                df_join.to_csv('data/activation/hidden_'+str(cont+1)+'-'+str(epoch)+'_activations.csv', header=None, index=None)
                #refre
                w = layer.get_weights()[0]
                b = layer.get_weights()[1]
                if os.path.isfile('data/weights/hidden_'+str(cont+1)+'-'+str(epoch)+'_weight.csv'):
                    os.remove('data/weights/hidden_'+str(cont+1)+'-'+str(epoch)+'_weight.csv')
                #input('conti 1')
                dfyw = pandas.DataFrame(w)
                dfyw.to_csv('data/weights/hidden_'+str(cont+1)+'-'+str(epoch)+'__weight.csv', header=None, index=None)
                #
                if os.path.isfile('data/bias/hidden_'+str(cont+1)+'-'+str(epoch)+'bia.csv'):
                    os.remove('data/bias/hidden_'+str(cont+1)+'-'+str(epoch)+'_bia.csv')
                dfyb = pandas.DataFrame(b)
                dfyb.to_csv('data/bias/hidden_'+str(cont+1)+'-'+str(epoch)+'_bia.csv', header=None, index=None)
                #refre
                rn, r = lot_executor(activaction, properties, Y)
                #refre
                layer_name.append(layer_type)
                metric_activaction_name.append(rn)
                layer_activaction.append(r)
                

            cont = cont + 1
    return  layer_name, metric_activaction_name, layer_activaction

def metric_executor(exe_amount, mode, properties, Y_train, Y_test, X_train, X_test):

    if(properties.interval() is None):
        raise Exception("Error, el intervalo utilizado para aplicar la métrica esta vacia.")
    
    if(properties.epochs() is None):
        raise Exception("Error, la cantidad de epochs no esta definido.")

    if(mode == 'train-test'):
        X = X_test
        Y = Y_test
    else:
        if(mode == 'train-train'):
            X = X_train
            Y = Y_train

    # Arreglo con los intervalos de epochs utilizados en la ejecución
    inter = range(properties.interval(),(properties.epochs()+properties.interval()),properties.interval())
    
    acc_results = [{} for x in inter]
    layers = [{} for x in inter]
    metric_name = [{} for x in inter]
    results = [{} for x in inter]
    models = [{} for x in inter]
    c = len(inter)
    for j in range(c):
        acc_results[j], models[j] = get_accurancy (exe_amount,'my_model', inter[j], properties, X, Y)

    for i in range(c):
        n = i+1
        print('Interval %s/%s' % (n,c))
        layers[i], metric_name[i], results[i] = metric(inter[i], properties, X, Y, models[i])

    return inter, acc_results, layers, metric_name, results