from initializers.custom_initializers import RandomUniform
import numpy as np
import tensorflow as tfv2
import tensorflow.compat.v1 as tf
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

def SVM(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['eta'] is None):
        raise Exception("Error, no posee learning rate del perceptron.")
    eta = prop['eta']

    if(prop['bias'] is None):
        raise Exception("Error, no posee bias.")
    bias = prop['bias']

    if(prop['C'] is None):
        raise Exception("Error, no posee C.")
    c = prop['C']

    #print ("acti shape size: " + str(len(acti_shape)))
    Y[Y == 0] = -1
    #w = RandomUniform(acti_shape, 0, 0.5)
    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction

    svm = SVC(kernel='linear', C=c, max_iter=epochs)
    svm.fit(X, np.ravel(Y))

    measure_accuracy = svm.predict(X)

    '''g = tf.Graph() 
    with g.as_default():
        batch_size = X.shape[0]
        x_data = tf.placeholder(shape=(X.shape[0],X.shape[1]), dtype=tf.float32)
        y_target = tf.placeholder(shape=(X.shape[0],1), dtype=tf.float32)
        A = tf.Variable(tfv2.random.uniform(
                [X.shape[1],1], 
                minval=0, 
                maxval=1, 
                dtype=tfv2.dtypes.float32, 
                seed=None, 
                name=None
            ))
        b = tf.Variable(tfv2.random.uniform(
                [1,1], 
                minval=0, 
                maxval=1, 
                dtype=tfv2.dtypes.float32, 
                seed=None, 
                name=None
            ))

        model_output = tf.subtract(tf.matmul(x_data, A), b)

        l2_norm = tf.reduce_sum(tf.square(A))
        alpha = tf.constant([0.1])
        classification_term = tf.reduce_mean(tf.maximum(0., tf.subtract(1.,tf.multiply(model_output, y_target))))
        loss = tf.add(classification_term, tf.multiply(alpha, l2_norm))

        prediction = tf.sign(model_output)
        accuracy = tf.reduce_mean(tf.cast(tf.equal(prediction, y_target), tf.float32))

        measure_call = tf.cast(tf.equal(prediction, y_target), tf.float32)
        
        my_opt = tf.train.GradientDescentOptimizer(0.01)
        train_step = my_opt.minimize(loss)

        init = tf.initialize_all_variables()

    with tf.Session(graph=g) as s:
        s.run(init)
        loss_vec = []
        measure_accuracy = []
        for i in range(epochs):
            rand_index = np.random.choice(len(X), size=batch_size)
            xx = X[rand_index]
            yy = Y[rand_index]
            s.run(train_step, feed_dict={x_data: xx, y_target: yy})
            temp_loss = s.run(loss, feed_dict={x_data: xx, y_target: yy})
            loss_vec.append(temp_loss)
            measure_accuracy_temp = s.run(measure_call, feed_dict={x_data: xx, y_target: yy})
            #print(np.count_nonzero(measure_accuracy_temp == tf.cast(0, tf.float32)))
            measure_accuracy.append(np.sum(measure_accuracy_temp == 0))'''
            
        #test_acc_temp = s.run(accuracy, feed_dict={x_data: xx, y_target: yy})
        #test_accuracy.append(test_acc_temp)
    y_pred = measureFunction(measure, measure_accuracy)

    return y_pred

def measureFunction (measure, measure_accuracy):
    if measure == "last":
        cant = measure_accuracy[-1]
    else:
        if measure == "higher":
            measure_accuracy.sort()
            cant = measure_accuracy[-1]
        else:
            if measure == "less":
                measure_accuracy.sort()
                cant = measure_accuracy[0]
            else:
                cant = measure_accuracy[-1]

    return cant