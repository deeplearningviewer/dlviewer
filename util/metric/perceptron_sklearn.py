import os
import numpy as np
import tensorflow.compat.v1 as tf
import tensorflow as tfv2
from sklearn.linear_model import Perceptron
from scipy.sparse import coo_matrix
from sklearn.utils import shuffle

'''from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y, predicted)'''

def contError (pred, y):
    cant = 0
    i = 0
    for n in pred:
        if y[i] != n:
            cant = cant + 1
        i += 1
    return cant

def perceptron_sklearn(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['eta'] is None):
        raise Exception("Error, no posee learning rate del perceptron.")
    eta = prop['eta']

    Y[Y == 0] = -1
    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction
    
    #'class_weight':[{0: w} for w in [1, 2, 4, 6, 10]]
    # auto
    # balanced

    perceptron = Perceptron(random_state = None, eta0=eta, class_weight = "balanced", shuffle=True)
    perceptron.fit(X, np.ravel(Y))

    measure_accuracy = []
    for epoch in range(epochs):
        
        pred = perceptron.predict(X)
        errcount = contError (pred, Y)
        measure_accuracy.append(errcount)

    #measure_call = tf.cast(tf.equal(predictions, data_y), tf.float32)
    r = measureFunction (measure, measure_accuracy)
                
    return r  


def measureFunction (measure, measure_accuracy):
    if measure == "last":
        cant = measure_accuracy[-1]
    else:
        if measure == "max":
            measure_accuracy.sort()
            cant = measure_accuracy[-1]
        else:
            if measure == "less":
                measure_accuracy.sort()
                cant = measure_accuracy[0]
            else:
                if measure == "prom":
                    c = 0 
                    nc = 0
                    for i in measure_accuracy:
                        nc += i
                        c += 1
                    cant = nc / c

    return cant