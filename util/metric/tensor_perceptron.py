import os
import numpy as np
import tensorflow.compat.v1 as tf
import tensorflow as tfv2

def contError (lst):
    cant = 0
    for n in lst:
        if n < 0.0:
            cant = cant + 1
    return cant

def tensor_perceptron(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['eta'] is None):
        raise Exception("Error, no posee learning rate del perceptron.")
    eta = prop['eta']

    Y[Y == 0] = -1
    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction
    
    r = 0
    g = tf.Graph() 
    with g.as_default():
        #X
        data_input_x = np.array(X)
        #Y
        data_y = np.array(Y)
        data_y[data_y == 0] = -1
        data_y = tf.cast(data_y, tf.float32)

        # Define the variables used during de processing
        # Two weights to only one neuron
        # Weights are initialized with zero
        weights = tf.Variable(tfv2.random.uniform(
            [data_input_x.shape[1],1], 
            minval=0, 
            maxval=1, 
            dtype=tfv2.dtypes.float32, 
            seed=None, 
            name=None
        ))
        
        bias = tf.Variable(tfv2.random.uniform(
            [1], 
            minval=0, 
            maxval=1, 
            dtype=tfv2.dtypes.float32, 
            seed=None, 
            name=None
        ))
        
        net_input = tf.matmul(data_input_x,weights) + bias
        # define our activation function to transform the output layer values into knowed classes (0 or 1)
        """Devolver clase usando función escalón de Heaviside.
        phi(z) = 1 si z >= theta; -1 en otro caso
        """
        predictions = tfv2.where(net_input >= 0.0, tf.cast(1, tf.float32), tf.cast(-1, tf.float32))

        # define score function to evaluate the accuracy
        error = tf.subtract(data_y, predictions)

        # define delta function used to adjust the weights during the training
        delta = tf.matmul(data_input_x, error, transpose_a = True)
        learningRate = eta
        train = tf.assign(weights, tf.add(weights, tf.multiply(delta, learningRate)))

        measure_call = tf.cast(tf.equal(predictions, data_y), tf.float32)

        # Create the initializer function TensorFlow Variables used during the processing
        init = tf.global_variables_initializer()

    with tf.Session(graph=g) as s:
        s.run(init)
        s.run(predictions)
        s.run(error)
        measure_accuracy = []
        for epoch in range(epochs):
            measure_accuracy_temp, _ = s.run([measure_call, train])
            measure_accuracy.append(np.sum(measure_accuracy_temp == 0))

            '''train_error_sum = tf.reduce_sum(train_error)
            s.run(train_error_sum)
            if train_error_sum.eval() == 0.0:
                break; # learned and got 100% accuracy
            '''
        r = measureFunction (measure, measure_accuracy)
                
    return r

def measureFunction (measure, measure_accuracy):
    if measure == "last":
        cant = measure_accuracy[-1]
    else:
        if measure == "max":
            measure_accuracy.sort()
            cant = measure_accuracy[-1]
        else:
            if measure == "less":
                measure_accuracy.sort()
                cant = measure_accuracy[0]
            else:
                if measure == "prom":
                    c = 0 
                    nc = 0
                    for i in measure_accuracy:
                        nc += measure_accuracy[i]
                        c += 1
                    cant = nc / c

    return cant