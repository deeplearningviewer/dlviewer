import os
import numpy as np
import tensorflow.compat.v1 as tf
import tensorflow as tfv2
from sklearn import linear_model

'''from sklearn.metrics import confusion_matrix
from sklearn.metrics import mean_squared_error, r2_score
cm = confusion_matrix(y, predicted)'''

def contError (pred, y):
    cant = 0
    i = 0
    for n in pred:
        if (y[i]* n) <= 0.0 :
            cant = cant + 1
        i += 1
    return cant

def linear_regression_sklearn(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['eta'] is None):
        raise Exception("Error, no posee learning rate del perceptron.")
    eta = prop['eta']

    Y[Y == 0] = -1
    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction
    
    # Create linear regression object
    regr = linear_model.LinearRegression()

    # Train the model using the training sets
    regr.fit(X, np.ravel(Y))

    measure_accuracy = []
    for epoch in range(epochs):
        pred = regr.predict(X)
        errcount = contError (pred, Y)
        measure_accuracy.append(errcount)
    
    r = measureFunction (measure, measure_accuracy)
                
    return r  


def measureFunction (measure, measure_accuracy):
    if measure == "last":
        cant = measure_accuracy[-1]
    else:
        if measure == "max":
            measure_accuracy.sort()
            cant = measure_accuracy[-1]
        else:
            if measure == "less":
                measure_accuracy.sort()
                cant = measure_accuracy[0]
            else:
                if measure == "prom":
                    c = 0 
                    nc = 0
                    for i in measure_accuracy:
                        nc += measure_accuracy[i]
                        c += 1
                    cant = nc / c

    return cant