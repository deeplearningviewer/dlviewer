import os
import numpy as np
import tensorflow.compat.v1 as tf
import tensorflow as tfv2
from scipy.optimize import linprog

'''from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y, predicted)'''

def contError (pred, y):
    cant = 0
    i = 0
    for n in pred:
        if n == True:
            t = -1
        else:
            t = 1
        if y[i] != t:
            cant = cant + 1
        i += 1
    return cant

def linear_programming_scipy(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['eta'] is None):
        raise Exception("Error, no posee learning rate del perceptron.")
    eta = prop['eta']

    Y[Y == 0] = -1
    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction  
    
    xx = np.array(Y.reshape(-1,1) * X)

    A_ub = np.append(xx, Y.reshape(-1,1), 1)

    b_ub = np.repeat(-1, A_ub.shape[0]).reshape(-1,1)

    c_obj = np.repeat(1, A_ub.shape[1])

    

    measure_accuracy = []
        
    res = linprog(c=c_obj, A_ub=A_ub, b_ub=b_ub,
                options={"disp": False})
    resultado = np.dot(A_ub, res["x"]) <= b_ub

    errcount = contError (resultado[0], Y)
    measure_accuracy.append(errcount)
    
    r = measureFunction (measure, measure_accuracy)
                
    return r  


def measureFunction (measure, measure_accuracy):
    if measure == "last":
        cant = measure_accuracy[-1]
    else:
        if measure == "max":
            measure_accuracy.sort()
            cant = measure_accuracy[-1]
        else:
            if measure == "less":
                measure_accuracy.sort()
                cant = measure_accuracy[0]
            else:
                if measure == "prom":
                    c = 0 
                    nc = 0
                    for i in measure_accuracy:
                        nc += measure_accuracy[i]
                        c += 1
                cant = nc / c

    return cant