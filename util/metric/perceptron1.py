import numpy as np
from initializers.custom_initializers import RandomUniform

class Perceptron:
    """Clasificador Perceptron basado en la descripción del libro
    "Python Machine Learning" de Sebastian Raschka.

    Parametros
    ----------

    eta: float
        Tasa de aprendizaje.
    n_iter: int
        Pasadas sobre el dataset.

    Atributos
    ---------
    w_: array-1d
        Pesos actualizados después del ajuste
    errors_: list
        Cantidad de errores de clasificación en cada pasada

    """
    def __init__(self, eta=0.1, n_iter=10, measure):
        self.eta = eta
        self.n_iter = n_iter
        self.prop = measure

    def fit(self, X, y):
        """Ajustar datos de entrenamiento

        Parámetros
        ----------
        X:  array like, forma = [n_samples, n_features]
            Vectores de entrenamiento donde n_samples es el número de muestras y
            n_features es el número de carácteristicas de cada muestra.
        y:  array-like, forma = [n_samples].
            Valores de destino

        Returns
        -------
        self:   object
        """
        self.w_ =  RandomUniform((1+X.shape[1]), 0, 0.5) #np.zeros(1 + X.shape[1])
        self.errors_ = []

        for _ in range(self.n_iter):
            errors = 0
            for xi, target in zip(X, y):
                update = self.eta * (target - self.predict(xi))
                self.w_[1:] += update * xi
                self.w_[0] += update
                errors += int(update != 0.0)
            self.errors_.append(errors)
        return self

    def getMeasure(self):
        measure = self.prop
        if(measure is None):
            raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

        if measure == "last":
            cant = self.errors_[-1]
        else:
            if measure == "higher":
                self.errors_.sort()
                cant = self.errors_[-1]
            else:
                if measure == "less":
                    self.errors_.sort()
                    cant = self.errors_[0]
                else:
                    cant = self.errors_[-1]
        return cant

    def predict(self, X):
        """Devolver clase usando función escalón de Heaviside.
        phi(z) = 1 si z >= theta; -1 en otro caso
        """
        phi = np.where(self.net_input(X) >= 0.0, 1, -1)
        return phi

    def net_input(self, X):
        """Calcular el valor z (net input)"""
        # z = w · x + theta
        z = np.dot(X, self.w_[1:]) + self.w_[0]
        return z

def contError (lst):
    cant = 0
    for n in lst:
        if n < 0.0:
            cant = cant + 1
    return cant

def perceptron1(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['eta'] is None):
        raise Exception("Error, no posee learning rate del perceptron.")
    eta = prop['eta']

    Y[Y == 0] = -1
    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction
    #w = RandomUniform(X[0].shape, 0, 0.5)
    
    # Inicializamos el perceptron
    ppn = Perceptron(eta=0.1, n_iter=10, measure)
    
    # Lo entrenamos con los vectores X e y
    ppn.fit(X, Y)

    # Se predice por el mismo dataset de entrenamiento
    pred = ppn.getMeasure()

    return pred
