import os
import numpy as np
import tensorflow.compat.v1 as tf
import tensorflow as tfv2
from sklearn.svm import SVC
from scipy.sparse import coo_matrix
from sklearn.utils import shuffle
from sklearn import preprocessing
from sklearn.multiclass import OneVsRestClassifier
from sklearn.ensemble import BaggingClassifier, RandomForestClassifier


def contError (pred, y):
    cant = 0
    i = 0
    for n in pred:
        #print("holaaaaa")
        #print(str(y[i])+" != "+ str(n))
        if y[i] != n:
            cant = cant + 1
        i += 1
    return cant

def svm_sklearn(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['C'] is None):
        raise Exception("Error, no posee C.")
    c = prop['C']

    if(prop['max_iter'] is None):
        raise Exception("Error, no posee max_iter.")
    max_iter = prop['max_iter']

    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction
    
    #scaler = preprocessing.MinMaxScaler()
    #X = scaler.fit(X).transform(X)
    n_estimators = 2
    svm = OneVsRestClassifier(BaggingClassifier(SVC(C=c, kernel="linear", probability = True, random_state=None, class_weight = "balanced", max_iter=max_iter), max_samples=1.0 / n_estimators, n_estimators=n_estimators))
    #svm = SVC(C=c, kernel="linear", probability = True, random_state=None, class_weight = "balanced", max_iter=max_iter)
    svm.fit(X, np.ravel(Y))

    measure_accuracy = []
    for epoch in range(epochs):

        #X_sparse = coo_matrix(X)
        #X, X_sparse, Y = shuffle(X, X_sparse, Y, random_state=None)
        
        #pred2 = svm.predict(X)
        pred = np.argmax(svm.predict_proba(X), axis=1)
        #print(svm.predict(X))
        #print(pred2)
        pred[pred == 0] = -1

        '''print("holaaaaa")
        print(pred)
        print(Y)'''
        errcount = contError (pred, Y)
        measure_accuracy.append(errcount)
    
    r = measureFunction (measure, measure_accuracy)     
    svm = None  
    return r  


def measureFunction (measure, measure_accuracy):
    if measure == "last":
        cant = measure_accuracy[-1]
    else:
        if measure == "max":
            measure_accuracy.sort()
            cant = measure_accuracy[-1]
        else:
            if measure == "less":
                measure_accuracy.sort()
                cant = measure_accuracy[0]
            else:
                if measure == "prom":
                    c = 0 
                    nc = 0
                    for i in measure_accuracy:
                        nc += i
                        c += 1
                    cant = nc / c

    return cant