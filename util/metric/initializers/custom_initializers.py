#from keras import backend as K
import numpy as np
def RandomUniform(shape, minval, maxval, seed=None):
    if(seed is not None):
        np.random.seed(seed)
    return np.random.uniform(minval,maxval,shape)
    #return K.random_uniform(shape, minval, maxval, seed, dtype)

#print(RandomUniform((58, 58, 32), 0, 0.5))