import os
import numpy as np
import tensorflow.compat.v1 as tf
import tensorflow as tfv2
from cvxopt import matrix, solvers

'''from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y, predicted)'''

def contError (lst):
    cant = 0
    for n in lst:
        if n < 0.0:
            cant = cant + 1
    return cant

def linear_programming_cvxopt(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['eta'] is None):
        raise Exception("Error, no posee learning rate del perceptron.")
    eta = prop['eta']

    Y[Y == 0] = -1
    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction  
    
    xx = np.array(Y.reshape(-1,1) * X)

    A_ub = np.append(xx, Y.reshape(-1,1), 1)

    b_ub = np.repeat(-1, A_ub.shape[0]).reshape(-1,1)

    c_obj = np.repeat(1, A_ub.shape[1])


    A = matrix(A_ub)
    b = matrix(b_ub)
    c = matrix(c_obj)


    sol=solvers.lp(c,A,b)
    
    measure_accuracy = sol['x']
    
    r = measureFunction (measure, measure_accuracy)
                
    return r  


def measureFunction (measure, measure_accuracy):
    if measure == "last":
        cant = measure_accuracy[-1]
    else:
        if measure == "higher":
            measure_accuracy.sort()
            cant = measure_accuracy[-1]
        else:
            if measure == "less":
                measure_accuracy.sort()
                cant = measure_accuracy[0]
            else:
                if measure == "average":
                    c = 0 
                    nc = 0
                    for i in measure_accuracy:
                        nc += measure_accuracy[i]
                        c += 1
                cant = nc / c

    return cant