from initializers.custom_initializers import RandomUniform
import numpy as np
import tensorflow as tfv2
import tensorflow.compat.v1 as tf
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

def SVMV2(activaction, prop, Y, measure):
    if(activaction is None):
        raise Exception("Error, las activaciones se encuntra vacia.")

    if(activaction[0] is None):
        raise Exception("Error, no se puede obtener la primera activacion.")
    acti = activaction[0]
    acti_shape = acti.shape

    if(measure is None):
        raise Exception("Error, la propiedades utilizadas para la medición no estan cargadas.")

    if(prop is None):
        raise Exception("Error, la propiedades utilizadas para el perceptron no estan cargadas.")
    
    if(prop['epochs'] is None):
        raise Exception("Error, no posee epochs definidos.")
    epochs = prop['epochs']

    if(prop['eta'] is None):
        raise Exception("Error, no posee learning rate del perceptron.")
    eta = prop['eta']

    if(prop['bias'] is None):
        raise Exception("Error, no posee bias.")
    bias = prop['bias']

    if(prop['C'] is None):
        raise Exception("Error, no posee C.")
    C = prop['C']

    if(prop['batch'] is None):
        raise Exception("Error, no posee batch.")
    batch = prop['batch']

    #print ("acti shape size: " + str(len(acti_shape)))
    Y[Y == 0] = -1
    #w = RandomUniform(acti_shape, 0, 0.5)
    if len(acti_shape) > 1:
        X = activaction.reshape(activaction.shape[0], -1)
    else:
        X = activaction
    r = 0
    g = tf.Graph() 
    with g.as_default():
        # Get the number of epochs for training.
        num_epochs = epochs
        # Get the C param of SVM
        svmC = C

        train_size = X.shape[0]
        BATCH_SIZE = batch

        x = tf.placeholder(shape=(X.shape[0],X.shape[1]), dtype=tf.float32)
        y = tf.placeholder(shape=(X.shape[0],1), dtype=tf.float32)

        W = tf.Variable(tfv2.random.uniform(
                [X.shape[1],1], 
                minval=0, 
                maxval=1, 
                dtype=tfv2.dtypes.float32, 
                seed=None, 
                name=None
            ))
        b = tf.Variable(tfv2.random.uniform(
                [1], 
                minval=0, 
                maxval=1, 
                dtype=tfv2.dtypes.float32, 
                seed=None, 
                name=None
            ))

        y_raw = tf.matmul(x,W) + b

        # Optimization.
        regularization_loss = 0.5*tf.reduce_sum(tf.square(W)) 
        hinge_loss = tf.reduce_sum(tf.maximum(tf.zeros([BATCH_SIZE,1]), 1 - y*y_raw))
        svm_loss = regularization_loss + svmC*hinge_loss
        train_step = tf.train.GradientDescentOptimizer(0.01).minimize(svm_loss)

        # Evaluation.
        predicted_class = tf.sign(y_raw)
        correct_prediction = tf.equal(y,predicted_class)
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        measure_call = tf.cast(correct_prediction, tf.float32)

        init = tf.initialize_all_variables()

    with tf.Session(graph=g) as s:
        s.run(init)
        measure_accuracy = []
        # Iterate and train.
        for step in range(num_epochs * train_size // BATCH_SIZE):   
            offset = (step * BATCH_SIZE) % train_size
            batch_data = X[offset:(offset + BATCH_SIZE), :]
            batch_labels = Y[offset:(offset + BATCH_SIZE)]
            train_step.run(feed_dict={x: batch_data, y: batch_labels})
            svm_loss.eval(feed_dict={x: batch_data, y: batch_labels})
            #print ('loss: ', svm_loss.eval(feed_dict={x: batch_data, y: batch_labels}))
            measure_accuracy_temp = s.run(measure_call, feed_dict={x: X, y: Y})
            measure_accuracy.append(np.sum(measure_accuracy_temp == 0))

        #accuracy.eval(feed_dict={x: X, y: Y})
        #print("Accuracy on train:", accuracy.eval(feed_dict={x: X, y: Y}))
        #eval_fun = lambda Xp: predicted_class.eval(feed_dict={x:Xp}); 
        #eval_fun = s.run(predicted_class, feed_dict={x: X, y: Y})
        
        r = measureFunction (measure, measure_accuracy)
    return r

def measureFunction (measure, measure_accuracy):
    if measure == "last":
        cant = measure_accuracy[-1]
    else:
        if measure == "max":
            measure_accuracy.sort()
            cant = measure_accuracy[-1]
        else:
            if measure == "less":
                measure_accuracy.sort()
                cant = measure_accuracy[0]
            else:
                if measure == "prom":
                    c = 0 
                    nc = 0
                    for i in measure_accuracy:
                        nc += measure_accuracy[i]
                        c += 1
                cant = nc / c
    return cant