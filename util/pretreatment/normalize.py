
def normalize_dataset(typeDataset = None,X_train_orig = None, Y_train_orig = None, X_test_orig = None, Y_test_orig = None):
    # dataset de caracteristicas
    if X_train_orig is None:
        raise Exception("Error, el train dataset de caracteristicas es nulo.")

    if X_test_orig is None:
        raise Exception("Error, el test dataset de caracteristicas es nulo.")
    
    # dataset de resultados
    if Y_train_orig is None:
        raise Exception("Error, el train dataset de resultados es nulo.")

    if Y_test_orig is None:
        raise Exception("Error, el test dataset de resultados es nulo.")

    if typeDataset == "h5":
        if len(X_train_orig.shape) == 3:
            X_train_orig = X_train_orig.reshape(X_train_orig.shape[0], X_train_orig.shape[1], X_train_orig.shape[2], 1)
            X_test_orig = X_test_orig.reshape(X_test_orig.shape[0], X_test_orig.shape[1], X_test_orig.shape[2], 1)
        
        print("Se normaliza los vectores de imagenes...")
        # Normalize image vectors
        X_train = X_train_orig/255.
        X_test = X_test_orig/255.

        # Reshape
        Y_train = Y_train_orig.T
        Y_test = Y_test_orig.T
        print("Se reforma la matriz...")

        if len(X_test.shape) == 2:
            shape = (X_test.shape[1],)
        else:
            if len(X_test.shape) == 3:
                shape = (X_test.shape[1], X_test.shape[2],1)
            else:
                shape = (X_test.shape[1],X_test.shape[2],X_test.shape[3])

    if typeDataset == "csv":
        X_train = X_train_orig
        X_test = X_test_orig

        Y_train = Y_train_orig.T
        Y_test = Y_test_orig.T
        
        shape = (X_test.shape[1],)

    print ("Cantidad de ejemplos de entrenamiento = " + str(X_train.shape[0]))
    print ("Cantidad de ejemplos de testeo = " + str(X_test.shape[0]))
    print ("Tamaño del X_train: " + str(X_train.shape))
    print ("Tamaño del Y_train: " + str(Y_train.shape))
    print ("Tamaño del X_test: " + str(X_test.shape))
    print ("Tamaño del Y_test: " + str(Y_test.shape))
    print ("Tamaño del del input: " + str(shape))

    return X_train, X_test, Y_train, Y_test, shape