
from util.architecture.dl_config import *
from util.pretreatment.kt_utils import *
from util.pretreatment.normalize import *
from util.construct_dnn import *
from util.metric import metric_executor
from keras.models import Model
from time import time
from util.optimization import optimization
import math
import csv
import os
import matplotlib.pyplot as plt

from util.result import *

def dnn_main():
    d= 0
    # Se instancia la clase encargada de obtener las propiedades.
    properties = ConfigParseLD("config.ini")
    


    wsave=None
    dnn_amount_accurancy = []
    dnn_amount_metric = []
    for i in (np.arange(int(properties.amount()))):
        print('::::::::::: Ejecucion Nro: --- ' + str(i+1)+' :::::::::::')

        train = None
        test = None 
        mode = properties.mode()
        print("Modo de ejecucion: "+ mode)

        # carga de los datasets 
        X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes = load_dataset(properties.databaset_type(), mode, properties.databaset(), properties.train(), properties.test(), properties.archives_attributes())
        print("Dataset de entrenamiento y testeo cargados....")

        # Se normaliza el dataset y se ve el tamaño
        X_train, X_test, Y_train, Y_test, shape = normalize_dataset(properties.databaset_type(), X_train_orig, Y_train_orig, X_test_orig, Y_test_orig)
        
        # Se genera el modelo, compila y entrena
        model, wsave = construct_dnn(str(i), properties, wsave, shape, X_train, Y_train)
        
        # Start counting.
        start_time = time()
        if(properties.metric_enable() == True):
            print("Medir por metrica")
            interval, acc, layers, metric_name, r = metric_executor(str(i), mode, properties, Y_train, Y_test, X_train, X_test)
            print(r)

        if(properties.layering_optimization() == True):
            print("Optimización de capas")
            optimization(str(i), mode, properties, Y_train, Y_test, X_train, X_test, model)
        # Calculate the elapsed time.
        elapsed_time = time() - start_time
        final_time = "%0.10f" % elapsed_time
        print('Elapsed time: ', str(str(final_time)+' seconds'))

        '''if properties.result_type() == "unique":
            resultUniqui(i, interval, acc, layers, metric_name, r, elapsed_time)
        else:
            if properties.result_type() == "measures_files":
                resultMeasuresFiles(i, interval, acc, layers, metric_name, r, elapsed_time)
            else:
                if properties.result_type() == "measures" or properties.result_type() == "measures_graphic":
                    if properties.result_type() == "measures":
                        resultMeasuresFiles(i, interval, acc, layers, metric_name, r, elapsed_time)
                    
                    #Se obtiene todos los porcentajes de aciertos de la métrica
                    epoch_amount_accurancy = []
                    #=== epoch - accuracy ===
                    cont_epoch = 0
                    for a in acc:
                        epoch_amount_accurancy.append([str('epoch '+ str(interval[cont_epoch])), str(a)])
                        cont_epoch += 1
                    #=== epoch - accuracy ===
                    dnn_amount_accurancy.append(epoch_amount_accurancy)
                    #=== Metrics Measurements ===
                    epoch_amount_metric = []
                    cont_metric = 0
                    for m_name in metric_name[0][0]:
                        
                        cont_layer = 0
                        for lay in layers[0]:
                            cont_interval = 0
                            mes =[]
                            layer_amount_metric = []
                            for inter in interval:
                                metric_value = r[cont_interval][cont_layer][cont_metric]
                                
                                #=== lot - value ===
                                x = len(metric_value)
                                if x > 0:
                                    me = 0
                                    for lot in metric_value:
                                        layer_amount_metric.append([str('epoch_m '+ str(inter)), lot])
                                #=== lot - value ===
                                cont_interval += 1
                            cont_layer += 1
                            epoch_amount_metric.append(layer_amount_metric)
                        cont_metric += 1
                    #=== Metrics Measurements ===

                    dnn_amount_metric.append(epoch_amount_metric)
    '''
    '''
    if properties.result_type() == "measures" or properties.result_type() == "measures_graphic":
        dnn_quantity = len(dnn_amount_metric)
        if dnn_quantity > 0:
            layer_quantity = len(dnn_amount_metric[0])
            if layer_quantity > 0:
                interval_quantity = len(dnn_amount_metric[0][0])

                print("dnn_quantity: "+ str(dnn_quantity))
                print("layer_quantity: "+ str(layer_quantity))
                print("interval_quantity: "+ str(interval_quantity))

                e = 0
                ep = 0
                while e < interval_quantity:
                    fig, ax = plt.subplots()
                    ax.set_xlim(0, layer_quantity)

                    #if properties.result_summary() == "extended":
                    print("Modo de resumen: extended")
                    ax.set_ylim(properties.result_ylim_start(), properties.result_ylim_end())
                    ax.xaxis.set_tick_params(which='minor', bottom=False)

                    #if properties.result_summary() == "summarized":
                    #    print("Modo de resumen: summarized")

                    start, end = ax.get_xlim()
                    ax.xaxis.set_ticks(np.arange(start, end+2, 1))
                    #x= np.arange(start=1, stop=layer_quantity+1, step=1)
                    
                    d = 1
                    for dnn in dnn_amount_metric:
                        dnn_epoch = []
                        l = 0
                        while l < layer_quantity:
                            nn = dnn[l][e]
                            dnn_epoch.append(nn[1])
                            l += 1
                        plt.plot(dnn_epoch, label='DNN'+str(d))
                        d += 1
                    
                    
                    ep += properties.interval()
                    plt.xlabel('CAPA')
                    plt.ylabel('PORCENTAJE DE ACIERTO')
                    plt.title("EPOCH "+str(ep))
                    plt.legend()

                    # Shrink current axis by 20%
                    box = ax.get_position()
                    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

                    # Put a legend to the right of the current axis
                    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

                    plt.show()
                    e += 1


    if properties.result_type() == "measures" or properties.result_type() == "measures_graphic":
        dnn_quantity = len(dnn_amount_metric)
        if dnn_quantity > 0:
            layer_quantity = len(dnn_amount_metric[0])
            if layer_quantity > 0:
                interval_quantity = len(dnn_amount_metric[0][0])
                e = 0
                ep = 0
                while e < interval_quantity:
                    fig, ax = plt.subplots()
                    ax.set_xlim(0, layer_quantity)

                    #if properties.result_summary() == "extended":
                    #    print("Modo de resumen: extended")
                    #    ax.set_ylim(properties.result_ylim_start(), properties.result_ylim_end())
                    #    ax.xaxis.set_tick_params(which='minor', bottom=False)

                    #if properties.result_summary() == "summarized":
                    #print("Modo de resumen: summarized")

                    start, end = ax.get_xlim()
                    ax.xaxis.set_ticks(np.arange(start, end+2, 1))
                    #x= np.arange(start=1, stop=layer_quantity+1, step=1)
                    
                    d = 1
                    for dnn in dnn_amount_metric:
                        dnn_epoch = []
                        l = 0
                        while l < layer_quantity:
                            nn = dnn[l][e]
                            dnn_epoch.append(nn[1])
                            l += 1
                        plt.plot(dnn_epoch, label='DNN'+str(d))
                        d += 1
                    
                    
                    ep += properties.interval()
                    plt.xlabel('CAPA')
                    plt.ylabel('PORCENTAJE DE ACIERTO')
                    plt.title("EPOCH "+str(ep))
                    plt.legend()

                    # Shrink current axis by 20%
                    box = ax.get_position()
                    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

                    # Put a legend to the right of the current axis
                    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

                    plt.show()
                    e += 1
    '''









