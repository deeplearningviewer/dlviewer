<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->




<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="#">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>
  <h3 align="center">Deep Learning Viewer</h3>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Tabla de Contenidos</summary>
  <ol>
    <li>
      <a href="#sobre-el-proyecto">Sobre el Proyecto</a>
      <ul>
        <li><a href="#dependencias">Dependencias</a></li>
      </ul>
    </li>
    <li>
      <a href="#inicio">Inicio</a>
    </li>
    <li><a href="#contacto">Contacto</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## Sobre el Proyecto

La herramienta busca analizar la separabilidad lineal de la red neuronal artificial por cada capa e intervalo de ejecución, para poder exponer valoraciones que reflejen el impacto que genera en la predicción cada capa. La herramienta genera dinamicamente modelos de redes neuronales artificiales y una configuración previa. Esto abstrae de tener conocimientos y nos ayuda a enfocarnos en la construcción de las arquitecturas. El algoritmo implementado posee otros parámetros importantes para el análisis y son los siguientes:

* Conjunto de datos: es el conjunto de datos a utilizar para alimentar el modelo en el entrenamiento.
* Modo de entrenamiento: se configura si la red va entrenar y testear con el mismo conjunto datos o se utiliza otro conjunto datos para el testeo.
* Intervalos de análisis: se configura cada cuantos intervalos se va ejecutar la herramienta.
* Tipo de salida: como se debe exponer los resultados.

Posee una métrica que esta totalmente desacoplada del método desarrollado, gracias a eso se puede utilizar esta herramienta para ejecutar análisis con distintas métricas. La herramienta también permite utilizar varias métricas al mismo tiempo para analizar las activaciones. 

<p align="right">(<a href="#top">back to top</a>)</p>



### Dependencias

Este proyecto fue desarrollado con los siguientes dependencias:

* [TensorFlow](https://www.tensorflow.org)
* [Keras](https://keras.io/)
* [Numpy](https://numpy.org/)
* [Pandas](https://pandas.pydata.org/)
* [Scipy](https://scipy.org/)
* [matplotlib](https://matplotlib.org/)
* [h5py](https://www.h5py.org/)
* [sklearn](https://scikit-learn.org)
* [tqdm](https://github.com/tqdm/tqdm)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Inicio

Para utilizar la herramienta se debe instalar todas las dependencias citas anteriormente, luego que se clone el proyecto del repositorio se debe modificar el archivo config.ini que es el lugar donde se asigna los tipos de capas y el archivo donde se encuentran todas las configuraciones de la arquitectura, conjunto de datos, modo de ejecución y métrica que debe ejecutar al momento de analizar.

Se contempla las capas más utilizadas y las propiedades de las mismas en un red convolucional que sigue un esquema secuencial, en caso que se quiera agregar más tipos de capas que no estan comtemplados en la herramienta se puede agregar desarrollando pequeños plugin de estas capas como se observa las capas en la carpeta layers.

Para iniciar la ejecución se debe ejecutar el archivo main.py que ejecuta la construcción dinámica de la arquitectura, entrenamiento del modelo y analizas los datos generado en las predicciones. 


<!-- CONTACT -->
## Contacto

Carlos Valdez - [LinkedIn](https://www.linkedin.com/in/carlos-valdez-85537016b) - carulises95@gmail.com

Link del proyecto: [https://gitlab.com/deeplearningviewer/dlviewer](https://gitlab.com/deeplearningviewer/dlviewer)

<p align="right">(<a href="#top">back to top</a>)</p>
